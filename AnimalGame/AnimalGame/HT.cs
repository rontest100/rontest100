﻿using System.Web;
using System.Web.Mvc;

public class HT
{
    public static string _sChk = "";
    public static string _sExcl = "";

    static HT()
    {
        _sChk  = char.ConvertFromUtf32(10003);
        _sExcl = char.ConvertFromUtf32(10071);
    }

    public static string Lbl(string s)
    {
        return HttpUtility.HtmlEncode(s).Replace("~", "<br>");
    }

    /*
    Public Shared Function LblTt(a_s As String()) As String
        Dim sLbl As String = a_s(0)
        Dim sTt As String = a_s(1)
        Dim nFldSort As Integer = 0
        If a_s.Length >= 3 Then
            Integer.TryParse(a_s(2), nFldSort)
        End If
        Dim sLblEnc As String = HttpUtility.HtmlEncode(sLbl).Replace("~", "<br>")
        Dim sTtEnc As String = HttpUtility.HtmlEncode(sTt).Replace("~", vbLf)
        Dim sRes As String = ""
        If nFldSort <> 0 Then
            sRes = String.Format("<span title=""{0}"" fs=""{1}"">{2}</span>", sTtEnc, nFldSort, sLblEnc)
        Else
            sRes = String.Format("<span title=""{0}"">{1}</span>", sTtEnc, sLblEnc)
        End If
        Return sRes
    End Function
    */

    public static string Int(int nVal)
    {
        return nVal.ToString();
    }

    /*
    Public Shared Function IntZ(nVal As Integer) As String
        If nVal = 0 Then
            Return Nothing
        End If
        Return CStr(nVal)
    End Function

    Public Shared Function IntN(nnVal As Integer?) As String
        If nnVal.HasValue Then
            Return CStr(nnVal.Value)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function Tt(sTt As String) As String
        If sTt & "" = "" Then
            Return ""
        Else
            Return String.Format("title=""{0}""",
                HttpUtility.HtmlEncode(sTt))
        End If
    End Function

    Public Shared Function IntN_Str(nnVal As Integer?, sTt As String) As String
        'If nnVal.HasValue Then
        '    Return CStr(nnVal.Value)
        'Else
        '    Return Nothing
        'End If
        Dim sVal As String = If(nnVal.HasValue, CStr(nnVal.Value), Nothing)
        If sTt & "" = "" Then
            Return sVal
        Else
            Return String.Format("<span title=""{0}"">{1}</span>",
                HttpUtility.HtmlEncode(sTt), sVal)
        End If
    End Function

    Public Shared Function IntNZ(nnVal As Integer?) As String
        If nnVal.HasValue AndAlso nnVal <> 0 Then
            Return CStr(nnVal.Value)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function Cur(decn As Decimal?) As String
        Return String.Format("{0:c}", decn)
    End Function

    Public Shared Function CurZ(decn As Decimal?) As String
        If decn.GetValueOrDefault = 0 Then
            Return Nothing
        End If
        Return String.Format("{0:c}", decn)
    End Function
    */

    public static string Str(string s)
    {
        return HttpUtility.HtmlEncode(s);
    }


    /*
    Public Shared Function StrB(s As String) As String
        If s Is Nothing Then
            Return Nothing
        End If
        s = s.Trim()
        If s = "" Then
            Return "&nbsp;"
        End If
        Return HttpUtility.HtmlEncode(s)
    End Function

    Public Shared Function Str_Str(s As String, sTt As String) As String
        Dim sEnc As String = HttpUtility.HtmlEncode(s)
        If sTt & "" = "" Then
            Return sEnc
        Else
            Return String.Format("<span title=""{0}"">{1}</span>",
                HttpUtility.HtmlEncode(sTt), sEnc)
        End If
    End Function

    Public Shared Function StrIfTrue(bTest As Boolean, s As String) As String
        If Not bTest Or s Is Nothing Then
            Return Nothing
        End If
        Return HttpUtility.HtmlEncode(s)
    End Function
    */

    public static string BoolChk(bool bVal)
    {
        return ((bVal) ? _sChk : "");
    }

    /*
    Public Shared Function Checked(b As Boolean) As String
    Return If(b, "checked", "")
    End Function

    Public Shared Function DateTime(dtn As DateTime?) As String
        Dim sDateTime As String = String.Format("{0:d/MM/yyyy~hh:mm:ss~tt~ddd}", dtn)
        Dim s As String = String.Format("{0}", HttpUtility.HtmlEncode(sDateTime).Replace("~", "&nbsp;"))
        Return s
    End Function

    Public Shared Function Date_DateTime(dtn As DateTime?) As String
        Dim sDate As String = String.Format("{0:d}", dtn)
        Dim sDateTime As String = String.Format("{0:d/MM/yyyy hh:mm:ss tt ddd}", dtn)
        Dim s As String = String.Format("<span title=""{0}"">{1}</span>",
            HttpUtility.HtmlEncode(sDateTime), HttpUtility.HtmlEncode(sDate))
        Return s
    End Function

    Public Shared Function Dbl(dVal As Double) As String
        Return String.Format("{0}", dVal)
    End Function

    Public Shared Function Dbl(dVal As Double, nAft As Integer) As String
        Return String.Format("{0:f" & nAft & "}", dVal)
    End Function

    Public Shared Function DblN(dnVal As Double?) As String
        Return String.Format("{0}", dnVal)
    End Function

    Public Shared Function DblN(dnVal As Double?, nAft As Integer) As String
        Return String.Format("{0:f" & nAft & "}", dnVal)
    End Function

    Public Shared Function DblNZ(dnVal As Double?) As String
        If dnVal.GetValueOrDefault = 0 Then
            Return Nothing
        End If
        Return String.Format("{0}", dnVal)
    End Function

    Public Shared Function DblNZ(dnVal As Double?, nAft As Integer) As String
        If dnVal.GetValueOrDefault = 0 Then
            Return Nothing
        End If
        Return String.Format("{0:f" & nAft & "}", dnVal)
    End Function

    Public Shared Function DblN_Str(dnVal As Double?, nAft As Integer, sTt As String) As String
        Dim s As String = String.Format("{0:f" & nAft & "}", dnVal)
        Dim sRes As String = String.Format("<span title=""{0}"">{1}</span>",
            HttpUtility.HtmlEncode(sTt), s)
        Return sRes
    End Function
    */

    /*
    Public Shared Sub Add(list_s As List(Of String), a_oFi As FldInfoBase(),
        nFldId As Integer, nHideBits As Integer,
        sFmt As String, ParamArray a_oArgs As Object())

        Dim oFi As FldInfoBase = a_oFi(nFldId)

        If (nHideBits And 1) <> 0 And (oFi.nHideBits And 1) <> 0 Then
            ' hide field
        Else
            list_s.Add(String.Format(sFmt, a_oArgs))
        End If
    End Sub
    */

    /*
    Public Shared Sub AddLbl(list_s As List(Of String), a_oFi As FldInfoBase(),
        nFldId As Integer, nHideBits As Integer,
        sFmt As String)

        Dim oFi As FldInfoBase = a_oFi(nFldId)

        If (nHideBits And 1) <> 0 And (oFi.nHideBits And 1) <> 0 Then
            ' hide field
        Else
            list_s.Add(String.Format(sFmt, HT.LblTt(oFi.sLblDescSort)))
        End If
    End Sub
    */

    /*
    Public Shared Function ClassRemAdd(sClassList As String, a_sClassRem As String(), a_sClassAdd() As String) As String
        Dim a_sClass As String() = sClassList.Split(" "c)
        Dim dict_sClass_b As Dictionary(Of String, Boolean) = New Dictionary(Of String, Boolean)
        For Each sClass As String In a_sClass
            If Not dict_sClass_b.ContainsKey(sClass) Then
                dict_sClass_b.Add(sClass, True)
            End If
        Next
        For Each sClass As String In a_sClassRem
            dict_sClass_b.Remove(sClass)
        Next
        For Each sClass As String In a_sClassAdd
            If Not dict_sClass_b.ContainsKey(sClass) Then
                dict_sClass_b.Add(sClass, True)
            End If
        Next
        Dim sClassListNew As String = String.Join(" "c, dict_sClass_b.Keys.ToArray)
        Return sClassListNew
    End Function
    */

}
