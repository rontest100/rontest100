﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnimalGame
{

    public partial class FormMain : Form
    {
        public static string sWebBaseDir
        {
            get
            {
                string sWebBaseDir_ = Directory.GetCurrentDirectory() + "\\..\\..\\Web";
                return sWebBaseDir_;
            }
        }

        public static string sWebBaseDir_Http
        {
            get
            {
                string sWebBaseDir_Http_ = String.Format("file:///{0}", sWebBaseDir.Replace("\\", "/"));
                return sWebBaseDir_Http_;
            }
        }

        private static string _sWebBaseDir_Http = sWebBaseDir_Http;

        public List<AnimalAttr> _list_oAnimalAttr = new List<AnimalAttr>();
        public List<AnimalName> _list_oAnimalName = new List<AnimalName>();

        public void UpdateAnimalView()
        {
            FillWb(_list_oAnimalAttr, _list_oAnimalName, wbMain);
        }

        public void FillWb(List<AnimalAttr> list_oAnimalAttr, List<AnimalName> list_oAnimalName, WebBrowser wb)
        {
            String sHtml = @"
<html>
<head>
<meta charset=""utf-8"">
<meta http-equiv=""X-UA-Compatible"" content=""IE=edge"">
<meta name=""viewport"" content=""width=device-width, initial-scale=1.0"">
<link href=""{{WEB}}/Content/bootstrap.css"" rel=""stylesheet""/>
<link href=""{{WEB}}/Content/site.css"" rel=""stylesheet""/>
<script type=""text/javascript"" src=""{{WEB}}/Scripts/jquery-1.10.2.js""></script>
<script type=""text/javascript"" src=""{{WEB}}/Scripts/common.js""></script>
<script type=""text/javascript"" src=""{{WEB}}/Scripts/common2.js""></script>
<style>
body {
    margin: 0px;
    padding: 10px;
}
.sui-eo-he {
    position: absolute;
    left: 0px;
    top: 0px;
    width: 100px;
    height: 100px;
    border: 1px solid #000000;
    overflow-x: hidden;
    overflow-y: scroll;
    border-bottom: none;
}
.sui-eo-de {
    width: 100%;
    /* height: 400px; */
    height: 400px;
    overflow: scroll;
    border: 1px solid #000000;
}
.cur-div {
    position: absolute;
    display: none;
    left: 0px;
    top: 0px;
    width: 0px;
    height: 0px;
    background: none;
    border: 1px solid #000000;
    pointer-events: none;
}
.S {
    color: red;
}
.U {

}
.a-u {
    text-decoration: underline;
}
</style>
<script lanuage=""javascript"">

//var nScrollTopMin = 40
var g_a_nIdSel = [0]
var g_nFldIdSel = -1
var g_a_sIdListChange = []

function OnBodyLoad ()
{
    SUI_Adj ()
}
function OnReSize()
{
    SUI_Adj ()
}
function SUI_Adj ()
{
    //var divDetScroll = O('SUI_divDetScroll')
    //divDetScroll.style.height = (window.innerHeight) + 'px'
    //SUI_AdjustHeader()
}
function SUI_OnHeadScroll(ev) {
    //LockedHeader_OnHeadScroll(ev, 'SUI')
}
function SUI_OnDetScroll(ev) {
    //LockedHeader_OnDetScroll(ev, 'SUI')
}
function SUI_AdjustHeader() {
    //LockedHeader_Adjust('SUI', '')
}
function S (ev, bDblClick)
{
    //alert ('S(): bDblClick (' + bDblClick + ')')

    //Sel_S ('SUI', ev, bDblClick)

	var tr = ev.currentTarget

    var sDataList = GA(tr, 'd')
    //alert (sDataList)

    var nFldId = 0
    Sel_DoEvSel (bDblClick, sDataList, nFldId)
}
function Chg_MarkTxtFld (td, txt) {
    var sValOrig = txt.getAttribute('o')
    var sValCur = txt.value
    $(td).removeClass('diff-k diff-e')
    if (sValCur != sValOrig) {
        dnValCur = DblNOf (sValCur)
        if (isNaN(dnValCur)) {
            $(td).addClass('diff-e')
        } else {            
            $(td).addClass('diff-k')
        }
    } else {
        //$(td).removeClass('diff-k diff-e')
    }
}
function C (ev) {
    Chg_C (ev)
}
function K (ev) {
    Chg_K (ev)
}
</script>
</head>
<body onload=""OnBodyLoad()"" onresize=""OnReSize()"" onkeydown=""Key_HandleArrowRet('SUI', event)"">

<table style=""display: none;"">
<tr>
<td><button type=""button"" name=""ev_butEv"" id=""ev_butEv""></button></td>
<td><input type=""hidden"" name=""ev_sEv"" id=""ev_sEv""></td>
<td><input type=""hidden"" name=""ev_bDblClick"" id=""ev_bDblClick""></td>
<td><input type=""hidden"" name=""ev_sIdList"" id=""ev_sIdList""></td>
<td><input type=""hidden"" name=""ev_sDataList"" id=""ev_sDataList""></td>
<td><input type=""hidden"" name=""ev_nFldId"" id=""ev_nFldId""></td>
<td><input type=""hidden"" name=""ev_nCellX"" id=""ev_nCellX""></td>
<td><input type=""hidden"" name=""ev_nCellY"" id=""ev_nCellY""></td>
<td><input type=""hidden"" name=""ev_nCellW"" id=""ev_nCellW""></td>
<td><input type=""hidden"" name=""ev_nCellH"" id=""ev_nCellH""></td>
<td><input type=""hidden"" name=""ev_bExpand"" id=""ev_bExpand""></td>
<td><input type=""hidden"" name=""ev_bChanges"" id=""ev_bChanges""></td>
</tr>
</table>

{{SCRIPTS}}

</body>
</html>
";

            List<String> list_sScripts = new List<String>();

            String sGridHtml1 = CreateSupplierInfoHtml(list_oAnimalAttr, list_oAnimalName);

            list_sScripts.Add(sGridHtml1);

            String sScripts = String.Join("\n\n", list_sScripts.ToArray());

            sHtml = sHtml.Replace("{{WEB}}", _sWebBaseDir_Http);
            sHtml = sHtml.Replace("{{SCRIPTS}}", sScripts);

            wb.DocumentText = sHtml;

        }

        public static void AnimalAttrHeader(List<string> list_s)
        {
            list_s.Add("<tr class=\"trh\">");

            list_s.Add(String.Format("<td class=\"tdh b-l\">{0}</td>", HT.Lbl("Select")));
            list_s.Add(String.Format("<td class=\"tdh b-l\">{0}</td>", HT.Lbl("Animal~Attribute")));

            list_s.Add("</tr>");
        }

        public static void AnimalNameHeader(List<string> list_s)
        {
            list_s.Add("<tr class=\"trh\">");

            list_s.Add(String.Format("<td class=\"tdh b-l\">{0}</td>", HT.Lbl("Select")));
            list_s.Add(String.Format("<td class=\"tdh b-l\">{0}</td>", HT.Lbl("Animal~Name")));
            list_s.Add(String.Format("<td class=\"tdh b-l\">{0}</td>", HT.Lbl("Animal~Attributes")));

            list_s.Add("</tr>");
        }

        public static string CreateSupplierInfoHtml(
            List<AnimalAttr> list_oAnimalAttr, List<AnimalName> list_oAnimalName)
        {
            List<string> list_s = new List<string>();

            list_s.Add("<p>Think of an animal...</p>");

            list_s.Add("<p>Check all the attributes that describes this animal:</p>");

            // Animal Attributes

            list_s.Add("<table class=\"tbl\" border=\"1\">");

            AnimalAttrHeader(list_s);

            foreach (AnimalAttr oAnimalAttr in list_oAnimalAttr)
            {
                list_s.Add(String.Format("<tr id=\"tr_{0}\" class=\"tr trsel0\" i=\"{1}\" d=\"{2}\" onclick=\"S(event,0)\">",
                    HT.Int(1), HT.Str("1"), HT.Str(oAnimalAttr.sAttr)));

                list_s.Add(String.Format("<td class=\"td t-c nw\">{0}</td>", HT.BoolChk(oAnimalAttr.bSet)));
                list_s.Add(String.Format("<td class=\"td t-l nw\">{0}</td>", HT.Str(oAnimalAttr.sAttr)));

                list_s.Add("</tr>");
            }

            list_s.Add("</table>");

            list_s.Add("<br>");

            list_s.Add("<p>The attributes selected match these animals:</p>");

            // Animal Name

            list_s.Add("<table class=\"tbl\" border=\"1\">");

            AnimalNameHeader(list_s);

            foreach (AnimalName oAnimalName in list_oAnimalName)
            {
                list_s.Add(String.Format("<tr id=\"tr_{0}\" class=\"tr trsel0\" i=\"{1}\" d=\"{2}\">",
                    HT.Int(1), HT.Str("1"), HT.Str("1")));

                list_s.Add(String.Format("<td class=\"td t-c nw\">{0}</td>", HT.BoolChk(oAnimalName.bSet)));
                list_s.Add(String.Format("<td class=\"td t-l nw\">{0}</td>", HT.Str(oAnimalName.sName)));
                list_s.Add(String.Format("<td class=\"td t-l nw\">{0}</td>", oAnimalName.GetAttrListHtml(list_oAnimalAttr)));

                list_s.Add("</tr>");
            }

            list_s.Add("</table>");

            string sFullAnimalName = AnimalName.GetAnimalFullName(list_oAnimalName);

            if (sFullAnimalName != "")
            {
                list_s.Add("<br>");

                list_s.Add(String.Format("<p>It's a(n) {0}</p>", sFullAnimalName));
            }

            list_s.Add("<br>");

            list_s.Add(String.Format("<p><i>Send only <a class=\"a-u\" href=\"http://www.whilewhat.com\" target=\"blank\">$39.95</a> and we will give you and 'Add' button for $free$.</i></p>", sFullAnimalName));

            string sHtml = String.Join("\n", list_s.ToArray());

            return sHtml;
        }


        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            _list_oAnimalName.Add(new AnimalName("Elephant", false, new List<string>(new String[] { "has a trunk", "grey in color", "trumpets" })));
            _list_oAnimalName.Add(new AnimalName("Lion", false, new List<string>(new String[] { "has a mane", "roars", "yellow in color", "eats meat" })));
            _list_oAnimalName.Add(new AnimalName("Cat", false, new List<string>(new String[] { "has cat eyes", "meows", "eats birds" })));
            _list_oAnimalName.Add(new AnimalName("Dog", false, new List<string>(new String[] { "has fury tail", "barks", "eats cats" })));
            _list_oAnimalName.Add(new AnimalName("Programmer", false, new List<string>(new String[] { "eats M&M's" })));

            _list_oAnimalAttr = AnimalName.GetUniqueAttrList(_list_oAnimalName);

            wbMain.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(wbSupplierInfo_DocumentCompleted);

            UpdateAnimalView();
        }

        private void wbSupplierInfo_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            /*
            Try
                wbSupplierInfo_DocumentCompleted_(sender, e)
            Catch ex As Exception
                App._Exception.HandleException(ex)
            End Try
            */
            wbSupplierInfo_DocumentCompleted_(sender, e);
        }
        private void wbSupplierInfo_DocumentCompleted_(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            //MessageBox.Show("wbSupplierInfo_DocumentCompleted_");

            WebBrowser wb = wbMain;

            // wbSupplierInfo.Document.Window.ScrollTo(_pntScrollPos)

            HtmlElement oElemId = wb.Document.All["ev_butEv"];
            oElemId.AttachEventHandler("onclick", butEv_Click);
        }

        private void butEv_Click(object sender, EventArgs e)
        {
            /*
            Try
                butEv_Click_(sender, e)
            Catch ex As Exception
                App._Exception.HandleException(ex)
            End Try
            */
            butEv_Click_(sender, e);
        }
        private void butEv_Click_(object sender, EventArgs e)
        {
            //MessageBox.Show("butEv_Click_");

            WebBrowser wb = wbMain;

            HtmlElement oElem_butEv = wb.Document.All["ev_butEv"];

            HtmlElement oElem_hid_sEv = wb.Document.All["ev_sEv"];
            string sEv = oElem_hid_sEv.GetAttribute("value");

            HtmlElement oElem_hid_sIdList = wb.Document.All["ev_sDataList"];
            String sDataList = oElem_hid_sIdList.GetAttribute("value");

            // _pntScrollPos = New Point(wb.Document.Body.ScrollLeft, wb.Document.Body.ScrollTop)

            //MessageBox.Show(sDataList);

            if (sEv == "S")
            {
                string sAttr = sDataList;
                AnimalAttr oAnimalAttr = AnimalAttr.FindByAttr(_list_oAnimalAttr, sAttr);
                if (oAnimalAttr != null)
                {
                    oAnimalAttr.bSet = !oAnimalAttr.bSet;

                    AnimalName.DoSelect(_list_oAnimalName, _list_oAnimalAttr);
                    UpdateAnimalView();
                }
            }

        }

        private void wbMain_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }
    }


    public class AnimalAttr
    {
        public AnimalAttr(string sAttr_, bool bSet_)
        {
            bSet = bSet_;
            sAttr = sAttr_;
        }

        public bool bSet = false;
        public string sAttr = "";

        public static AnimalAttr FindByAttr(List<AnimalAttr> list_oAnimalAttr, string sAttr)
        {
            foreach (AnimalAttr oAnimalAttr in list_oAnimalAttr)
            {
                if (oAnimalAttr.sAttr == sAttr)
                {
                    return oAnimalAttr;
                }
            }
            return null;
        }

        public static List<string> GetSelectedAttrList(List<AnimalAttr> list_oAnimalAttr)
        {
            List<string> list_sAttr = new List<string>();
            foreach (AnimalAttr oAnimalAttr in list_oAnimalAttr)
            {
                if (oAnimalAttr.bSet)
                {
                    list_sAttr.Add(oAnimalAttr.sAttr);
                }
            }
            return list_sAttr;
        }
    }

    public class AnimalName
    {
        public AnimalName(string sName_, bool bSet_, List<string> list_sAttr_)
        {
            bSet = bSet_;
            sName = sName_;
            list_sAttr = list_sAttr_;
        }

        public bool bSet = false;
        public string sName = "";
        public List<string> list_sAttr = new List<string>();

        public string sAttrList
        {
            get
            {            
                string sAttrList_ = String.Join(", ", list_sAttr.ToArray());
                return sAttrList_;
            }
        }

        public string GetAttrListHtml (List<AnimalAttr> list_oAnimalAttr)
        {
            List<string> list_sAttrDisp = new List<string>();
    
            List<string> list_sSelectedAttr = AnimalAttr.GetSelectedAttrList(list_oAnimalAttr);
    
            foreach (string sAttr in list_sAttr)
            {
                if (list_sSelectedAttr.Contains(sAttr)) {
                    list_sAttrDisp.Add(String.Format("<span class=\"S\">{0}</span>", sAttr));
                } else {
                    list_sAttrDisp.Add(String.Format("<span class=\"U\">{0}</span>", sAttr));
                }
            }

            string sAttrListHtml_ = String.Join(", ", list_sAttrDisp.ToArray());
            return sAttrListHtml_;
        }

        public static void ClearSelect(List<AnimalName> list_oAnimalName)
        {
            foreach (AnimalName oAnimalName in list_oAnimalName)
            {
                oAnimalName.bSet = false;
            }
        }

        public static void DoSelect(List<AnimalName> list_oAnimalName, List<AnimalAttr> list_oAnimalAttr)
        {
            ClearSelect(list_oAnimalName);
            foreach (AnimalName oAnimalName in list_oAnimalName)
            {
                foreach (AnimalAttr oAnimalAttr in list_oAnimalAttr)
                {
                    if (oAnimalAttr.bSet)
                    {
                        if (oAnimalName.list_sAttr.Contains(oAnimalAttr.sAttr))
                        {
                            oAnimalName.bSet = true;
                        }
                    }
                }
            }
        }

        public static List<AnimalAttr> GetUniqueAttrList(List<AnimalName> list_oAnimalName)
        {
            List<AnimalAttr> list_oAnimalAttr = new List<AnimalAttr>();

            Dictionary<string, bool> dict_sAttr_b = new Dictionary<string, bool>();
            foreach (AnimalName oAnimalName in list_oAnimalName)
            {
                foreach (string sAttr in oAnimalName.list_sAttr)
                {
                    if (!dict_sAttr_b.ContainsKey(sAttr))
                    {
                        dict_sAttr_b[sAttr] = true;
                    }
                }
            }

            List<string> list_sAttr = new List<string>(dict_sAttr_b.Keys);
            list_sAttr.Sort();

            foreach (string sAttr in list_sAttr)
            {
                AnimalAttr oAnimalAttr = new AnimalAttr(sAttr, false);
                list_oAnimalAttr.Add(oAnimalAttr);
            }

            return list_oAnimalAttr;
        }

        public static string GetAnimalFullName(List<AnimalName> list_oAnimalName)
        {
            List<string> list_sName = new List<string>();
            foreach (AnimalName oAnimalName in list_oAnimalName)
            {
                if (oAnimalName.bSet)
                {
                    list_sName.Add(oAnimalName.sName);
                }
            }

            string sFullName = String.Join("-", list_sName);
            return sFullName;
        }

    }
}
