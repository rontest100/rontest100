// see www.whilewhat.com for info

function SE(ex) {
	alert ('Javascript Exception:\n\n' + ex.stack)
}

var Com_bIsFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1

function O(sId) {
	try {
	    return document.getElementById(sId)
	} catch (ex) {SE(ex);throw ex}
}
function GA(oElem, sAttr) {
	try {
	    return oElem.getAttribute(sAttr)
	} catch (ex) {SE(ex);throw ex}
}
function SA(oElem, sAttr, sVal) {
	try {
	    oElem.setAttribute(sAttr, sVal)
	} catch (ex) {SE(ex);throw ex}
}
function OD(obj) {
	try {
		var list_sName = []
		for (var sName in obj) {
			list_sName.push (sName)
		}
		list_sName.sort()
		var sNameList = list_sName.join(' ')
		alert (sNameList)
	} catch (ex) {SE(ex);throw ex}
}
function ODC(obj, sCont) {
	try {
		var list_sName = []
		for (var sName in obj) {
			if (sName.indexOf (sCont) != -1) {
				list_sName.push (sName)
			}
		}
		list_sName.sort()
		var sNameList = list_sName.join(' ')
		alert (sNameList)
	} catch (ex) {SE(ex);throw ex}
}
function AD(a) {
	try {
		var list_sIndexVal = []
		for (var sIndex in a) {
			list_sIndexVal.push(sIndex + ':' + a[sIndex])
		}
		var sIndexValList = list_sIndexVal.join(', ')
		alert(sIndexValList)
	} catch (ex) {SE(ex);throw ex}
}
function Trim (sStr)
{
	try {
		var sRes = sStr.replace (/^\s+|\s+$/g, '')
		return sRes
	} catch (ex) {SE(ex);throw ex}
}
function LTrim (sStr)
{
	try {
		var sRes = sStr.replace (/^\s+/, '')
		return sRes
	} catch (ex) {SE(ex);throw ex}
}
function RTrim (sStr)
{
	try {
		var sRes = sStr.replace (/\s+$/, '')
		return sRes
	} catch (ex) {SE(ex);throw ex}
}
function CompInt(n1, n2) {
	try {
	    return n1 - n2
	} catch (ex) {SE(ex);throw ex}
}
function AssocArrEmpty(a) {
	try {
		var bEmpty = true
		for (var sIndex in a) {
			bEmpty = false
			break
		}
		return bEmpty
	} catch (ex) {SE(ex);throw ex}
}
function ArrFromAssocIndex(a) {
	try {
		var list_sIndex = []
		for (var sIndex in a) {
			list_sIndex.push(sIndex)
		}
		return list_sIndex
	} catch (ex) {SE(ex);throw ex}
}
function ArrFromAssocNegFill(a, nValDef) {
	try {
		var a_nVal = []
		var nIndexLast = 0
		for (var sIndex in a) {
			var nIndex = parseInt(sIndex)
			if (nIndex < nIndexLast) {
				nIndexLast = nIndex
			}
		}
		for (nIndex = -1; nIndex >= nIndexLast; nIndex--) {
			var nVal = a[nIndex]
			if (typeof (nVal) == 'undefined') {
				nVal = nValDef
			}
			a_nVal.push(nVal)
		}
		return a_nVal
	} catch (ex) {SE(ex);throw ex}
}
function ArrFindFirstDup(a_nVal, nValDef) {
	try {
		var nCount = a_nVal.length
		var a_nValSort = []
		for (n = 0; n < nCount; n++) {
			var nVal = a_nVal[n]
			if (nVal != nValDef) {
				a_nValSort.push(nVal)
			}
		}
		a_nValSort.sort(CompInt)

		nCount = a_nValSort.length
		var nValPrev = null
		for (n = 0; n < nCount; n++) {
			var nVal = a_nValSort[n]
			if (nValPrev != null && nVal == nValPrev) {
				return nVal
			}
			nValPrev = nVal
		}
		return -1
	} catch (ex) {SE(ex);throw ex}
}
function ArrFindFirstNotContig(a_nVal, nValStart) {
	try {
		var nCount = a_nVal.length
		var a_nValSort = []
		for (var n = 0; n < nCount; n++) {
			var nVal = a_nVal[n]
			if (nVal >= nValStart) {
				a_nValSort.push(nVal)
			}
		}
		a_nValSort.sort(CompInt)

		nCount = a_nValSort.length
		var nValPrev = nValStart - 1
		for (var n = 0; n < nCount; n++) {
			var nVal = a_nValSort[n]
			if (nVal - nValPrev != 1) {
				return nVal
			}
			nValPrev = nVal
		}
		return -1
	} catch (ex) {SE(ex);throw ex}
}
function Enc(s) {
	try {
		s = s.replace(/~/g, '~t')
		s = s.replace(/'/g, '~q')
		return s
	} catch (ex) {SE(ex);throw ex}
}
function Dec(s) {
	try {
		s = s.replace(/~q/g, "'")
		s = s.replace(/~t/g, '~')
		return s
	} catch (ex) {SE(ex);throw ex}
}
function H(s) {
	try {
		s = s.replace(/&/g, '&amp;')
		s = s.replace(/</g, '&lt;')
		s = s.replace(/>/g, '&gt;')
		return s
	} catch (ex) {SE(ex);throw ex}
}
function UH(s) {
	try {
		s = s.replace(/&gt;/g, '>')
		s = s.replace(/&lt;/g, '<')
		s = s.replace(/&amp;/g, '&')
		return s
	} catch (ex) {SE(ex);throw ex}
}
function IntOf(s) {
	try {
		s = s.replace(/\s*/, '')
		if (s == '') {
			return 0
		}
		if (! /^\d+$/.test(s)) {
			return NaN
		}
		var n = parseInt(s, 10)
		return n
	} catch (ex) {SE(ex);throw ex}
}
function DblOf(s) {
	try {
		s = s.replace(/\s*/, '')
		if (s == '') {
			return 0.0
		}
		if (! /^\d+(\.\d{1,2})?$/.test(s)) {
			return NaN
		}
		var d = parseFloat(s)
		return d
	} catch (ex) {SE(ex);throw ex}
}
function DblNOf(s) {
	try {
		s = s.replace(/\s*/, '')
		if (s == '') {
			return null
		}
		if (! /^\d+(\.\d*)?$/.test(s)) {
			return NaN
		}
		var d = parseFloat(s)
		return d
	} catch (ex) {SE(ex);throw ex}
}
function IntOr0(s) {
	try {
		var n = parseInt(s, 10)
		if (isNaN(n)) {
			return 0
		}
		return n
	} catch (ex) {SE(ex);throw ex}
}
function Str01(b) {
	try {
	    return ((b) ? '1' : '0')
	} catch (ex) {SE(ex);throw ex}
}
function Int01(b) {
	try {
	    return ((b) ? 1 : 0)
	} catch (ex) {SE(ex);throw ex}
}
// see www.whilewhat.com (Ron)
function Trunc(dIn) {
	try {
		return (dIn < 0) ?
			Math.ceil(dIn) :
			Math.floor(dIn)
	} catch (ex) {SE(ex);throw ex}
}
function DoTab(oInputTarget, bNext) {
	try {
		var a_nFullTabIndex = []
		var aa_nFullTabIndex_oInput = []

		var nMinorIndex = 1
		var nFullTabIndexTarget = 0
		$('input, select').each(function (n, oInput) {
			if (Com_bIsFirefox) {
				if (oInput.tagName == 'SELECT') {
					return
				}
			}
			if (oInput.type == 'button' || oInput.type == 'submit' || oInput.type == 'hidden') {
				return
			}
			var sDisplay = window.getComputedStyle(oInput).display
			if (sDisplay == 'none') {
				return
			}
			var nFullTabIndex = oInput.tabIndex * 1000 + nMinorIndex
			a_nFullTabIndex.push(nFullTabIndex)
			aa_nFullTabIndex_oInput[nFullTabIndex] = oInput
			if (oInput == oInputTarget) {
				nFullTabIndexTarget = nFullTabIndex
			}
			nMinorIndex++
		})

		a_nFullTabIndex.sort(CompInt)

		var nFullTabIndexFound = null

		if (bNext) {
			for (var n = 0; n < a_nFullTabIndex.length; n++) {
				var nFullTabIndex = a_nFullTabIndex[n]
				if (nFullTabIndexTarget == 0 || nFullTabIndex > nFullTabIndexTarget) {
					//alert(1)
					nFullTabIndexFound = nFullTabIndex
					break
				}
			}
		} else {
			for (var n = a_nFullTabIndex.length - 1; n >= 0; n--) {
				var nFullTabIndex = a_nFullTabIndex[n]
				if (nFullTabIndexTarget == 0 || nFullTabIndex < nFullTabIndexTarget) {
					nFullTabIndexFound = nFullTabIndex
					break
				}
			}
		}

		if (nFullTabIndexFound != null) {
			var oInputNext = aa_nFullTabIndex_oInput[nFullTabIndexFound]
			if (oInputNext.type == 'checkbox' || oInputNext.tagName == 'SELECT') {
				if (document.activeElement) {
					var oElem = document.activeElement
					if (oElem.tagName == 'INPUT' && oElem.type == 'text') {
						oElem.selectionStart = 0
						oElem.selectionEnd = 0
					}
				}
				oInputNext.focus()
			} else {
				oInputNext.focus()
				oInputNext.select()
			}
		}
	} catch (ex) {SE(ex);throw ex}
}
function OnClr (oImg)
{
	try {
		var oInput = oImg.previousSibling
		oInput.value = ''
		if (oInput.onchange) {
			oInput.onchange()
		}
		oInput.selectionStart = 0
		oInput.selectionEnd = 0
		oInput.focus()
	} catch (ex) {SE(ex);throw ex}
}
function AddElemClr(oInput, sBaseUrl) {
	try {
		var oJqInput = $(oInput)
		oJqInput.after('<img class="clr" src="~/img/clr_t.png" onclick="OnClr(this)">'.replace('~', sBaseUrl))
	} catch (ex) {SE(ex);throw ex}
}
function AddClr(sBaseUrl, sTopElemId) {
	try {
		var oJqExpr = null
		if (typeof (sTopElemId) != 'undefined') {
			var oTopElem = O(sTopElemId)
			oJqExpr = $(oTopElem).find('input')
		} else {
			oJqExpr = $('input')
		}

		oJqExpr.each(function (nIndex, oInput) {
			if (!(oInput.type == 'text' || oInput.type == 'password')) {
				return
			}
			if (oInput.disabled) {
				return
			}
			var sDisplay = window.getComputedStyle(oInput).display
			if (sDisplay == 'none') {
				return
			}
			AddElemClr(oInput, sBaseUrl)
		})
	} catch (ex) {SE(ex);throw ex}
}
function AddKey(sBaseUrl, sTopElemId) {
	try {
		var oJqExpr = null
		if (typeof (sTopElemId) != 'undefined') {
			var oTopElem = O(sTopElemId)
			oJqExpr = $(oTopElem).find('input, select')
		} else {
			oJqExpr = $('input, select')
		}

		oJqExpr.keydown(function (event) {
			//alert ('In AddKey()')
			var oInputTarget = this
			if (event.keyCode == 38) {
				event.preventDefault()
				DoTab(oInputTarget, false)
				return false
			}
			if (event.keyCode == 40 || event.keyCode == 13) {
				event.preventDefault()
				DoTab(oInputTarget, true)
				return false
			}
			return true
		})
	} catch (ex) {SE(ex);throw ex}
}
function AddSpec(sBaseUrl, sTopElemId) {
	try {
		AddClr(sBaseUrl, sTopElemId)
		AddKey(sBaseUrl, sTopElemId)
	} catch (ex) {SE(ex);throw ex}
}
function Str_MsgIf(bChk, s, sMsg) {
	try {
		if (bChk) {
			return '<span class="enter">' + H(sMsg) + '</span>'
		}
		return H(s)
	} catch (ex) {SE(ex);throw ex}
}
function SetTab(nTabSet, a_a_Tab) {
	try {
		var nTabCount = a_a_Tab.length
		for (nTab = 0; nTab < nTabCount; nTab++) {
			var a_Tab = a_a_Tab[nTab]
			var tab = O(a_Tab[0])
			var div = O(a_Tab[1])
			tab.className = tab.className.replace(/td-tabs/, 'td-tabu')
			div.style.display = 'none'
		}
		for (nTab = 0; nTab < nTabCount; nTab++) {
			if (nTab == nTabSet) {
				var a_Tab = a_a_Tab[nTab]
				var tab = O(a_Tab[0])
				var div = O(a_Tab[1])
				tab.className = tab.className.replace(/td-tabu/, 'td-tabs')
				div.style.display = 'block'
			}
		}
	} catch (ex) {SE(ex);throw ex}
}
function SetTrSel(trName, bSel)
{
	try {
		var tr = O(trName)
		if (tr != null) {
			if (bSel) {
				tr.className = tr.className.replace(/trsel0/, 'trsel1')
			} else {
				tr.className = tr.className.replace(/trsel1/, 'trsel0')
			}
			return true
		}
		return false
	} catch (ex) {SE(ex);throw ex}
}
function AddEditNavKeys(sElemName) {
	try {
		$('#' + sElemName).keydown(function (event) {
			//alert ('In AddEditNavKeys ()')
			var oInputTarget = this

			//alert(event.keyC)

			var bHandle = false
			switch (event.keyCode) {
				case 38: // up
					EditNavKeys_FldUp()
					bHandle = true
					break
				case 40: // down
				case 13: // enter
					EditNavKeys_FldDown()
					bHandle = true
					break
				case 37: // left
					EditNavKeys_FldLeft()
					bHandle = true
					break
				case 39: // right
					EditNavKeys_FldRight()
					bHandle = true
					break
			}

			if (bHandle) {
				event.preventDefault()
				//alert('handle')
				return false
			}

			return true
		})
	} catch (ex) {SE(ex);throw ex}
}
function Data_Chk(s) {
	try {
	    return (s == '') ? '' : '1'
	} catch (ex) {SE(ex);throw ex}
}
function Data_Id(s) {
	try {
	    return (s == '' || s == '0') ? '' : s
	} catch (ex) {SE(ex);throw ex}
}
function Data_Str(s) {
	try {
		s = s.replace(/\s*/, '')
		s = s.replace(/~/g, '~t')
		s = s.replace(/'/g, '~q')
		s = s.replace(/\|/g, '~b')
		s = s.replace(/</g, '~l')
		s = s.replace(/>/g, '~g')
		s = s.replace(/&/g, '~a')
		return s
	} catch (ex) {SE(ex);throw ex}
}
function AddTt(sTopElemId) {
	try {
		//$('span[title]').click(ShowTt)
		//$('#divTt').click(HideTt)
		//alert("AddTt(" + sTopElemId + ")")
		var oJqExpr = null
		if (typeof (sTopElemId) != 'undefined') {
			var oTopElem = O(sTopElemId)
			oJqExpr = $(oTopElem).find('span[title]')
		} else {
			oJqExpr = $('span[title]')
			var divTt = $('#divTt')
			divTt.click(HideTt)
		}
		oJqExpr.click(ShowTt)
		oJqExpr.each(function () {
			//console.log(this.parentElement.tagName)
			if (this.parentElement.tagName == 'TD') {
				$(this.parentElement).addClass('ttm')
			}
		})
	} catch (ex) {SE(ex);throw ex}
}
function FldSort_FindSortFunc(span)
{
	try {
		var oElem = span
		for (var n = 1; n <= 3; n++) {
			var oElem = oElem.parentElement
			if (oElem != null) {
				var sSortFunc = oElem.getAttribute('fs-f')
				if (sSortFunc != null) {
					return sSortFunc
				}
			}
		}
		return ''
	} catch (ex) {SE(ex);throw ex}
}
function FldSort_UpdateSortMark(sSection, nFldSort) {
	try {
		$('#' + sSection + '_divSfU,#' + sSection + '_divSfD').hide()

		if (nFldSort == 0) {
			return
		}

		var nFldSortAbs = Math.abs(nFldSort)
		var sUD = ((nFldSort < 0) ? 'U' : 'D')

		var tblHeader = O(sSection + '_tblHead')
		//var tblHeader_rect = tblHeader.getBoundingClientRect()

		var tblHeadScroll = O(sSection + '_divHeadScroll')
		var tblHeadScroll_rect = tblHeadScroll.getBoundingClientRect()

		var a_spanHeaderFld = $(tblHeader).find('span[fs=' + nFldSortAbs + ']')
		if (a_spanHeaderFld.length == 0) {
			return
		}
		var spanHeaderFld = a_spanHeaderFld[0]
		var tdHeaderFld = spanHeaderFld.parentElement
		var tdHeaderFld_rect = tdHeaderFld.getBoundingClientRect()

		var divSf = O(sSection + '_divSf' + sUD)

		var sBordL = getComputedStyle(tblHeadScroll, null).borderLeftWidth
		sBordL = sBordL.replace('px', '')
		var nBordL = IntOr0(sBordL)

		var sBordT = getComputedStyle(tblHeadScroll, null).borderTopWidth
		sBordT = sBordT.replace('px', '')
		var nBordT = IntOr0(sBordT)

		//divSf.style.left = (tdHeaderFld_rect.left - tblHeadScroll_rect.left + window.pageXOffset - nBordL) + 'px'
		//divSf.style.top = (tdHeaderFld_rect.top - tblHeadScroll_rect.top + window.pageYOffset - nBordT) + 'px'

		divSf.style.left = (tdHeaderFld_rect.left - tblHeadScroll_rect.left - nBordL) + 'px'
		divSf.style.top = (tdHeaderFld_rect.top - tblHeadScroll_rect.top - nBordT) + 'px'

		divSf.style.width = tdHeaderFld_rect.width + 'px'
		divSf.style.height = tdHeaderFld_rect.height + 'px'

		$(divSf).show()
	} catch (ex) {SE(ex);throw ex}
}
function ShowTt(ev) {
	try {
		//alert('ShowTt()')

		var span = ev.target

		if (C_bToolTips) {
			var divTt = O('divTt')
			var span_rect = span.getBoundingClientRect()

			var sSpanTitle = span.title
			sSpanTitle = sSpanTitle.replace(/\n/g, '<br>')

			divTt.innerHTML = '<div id="divT" class="tt-divT"></div><span id="spanTt" class="tt-span">' + sSpanTitle + '</span>'
			var divT = O('divT')
			var spanTt = O('spanTt')

			divTt.style.width = '200px'
			divTt.style.height = '20px'

			divTt.style.left = (span_rect.left + window.pageXOffset) + "px"
			divTt.style.top = (span_rect.top + window.pageYOffset + span_rect.height) + "px"

			divTt.style.display = 'block'

			var spanTt_rect = spanTt.getBoundingClientRect()

			var nPad = 2
			var nPad2 = nPad * 2

			divTt.style.width = (spanTt_rect.width + 6 + nPad2) + 'px'
			divTt.style.height = (spanTt_rect.height + 6 + nPad2) + 'px'
			divT.style.height = (nPad) + 'px'

			//ev.preventDefault()
		} else {
			var nFldSort = span.getAttribute('fs')
			var sFldSortFunc = FldSort_FindSortFunc(span)
			//alert('nFldSort (' + nFldSort + ')')
			//alert('sFldSortFunc (' + sFldSortFunc + ')')

			if (nFldSort != null && sFldSortFunc != null) {
				ShowTtS(ev, 'a', nFldSort, sFldSortFunc)
				ShowTtS(ev, 'd', -nFldSort, sFldSortFunc)
			}
		}
	} catch (ex) {SE(ex);throw ex}
}
function ShowTtS(ev, sDir, nFldSort, sFldSortFunc) {
	try {
		//alert("ShowTtS ('" + sDir + "'," + nFldSort + ") x")

		var span = ev.target
		var span_rect = span.getBoundingClientRect()

		var divTt = O('divTt')
		var divTtS = O('divTtS' + sDir)
		var divTtSG = O('divTtSG' + sDir)
		var divTt_rect = divTt.getBoundingClientRect()

		//var sSpanTitle = ((sDir == 'a') ? 'A\n&#x2b07;\nZ' : 'Z\n&#x2b06;\nA')
		//var sSpanTitle = ((sDir == 'a') ? '&#x2b07;' : '&#x2b06;')
		var sUpDn = ((sDir == 'a') ? 'Dn' : 'Up')
		var sSpanTitle = '<img id="imgTS' + sDir + '" src="../img/Arrow' + sUpDn + 'Red_8_12.png" width="10" height="14">'

		// 'ArrowUpRed_8_12.png'
		sSpanTitle = sSpanTitle.replace(/\n/g, '<br>')

		divTtS.innerHTML = '<div onclick="' + sFldSortFunc + '(' + nFldSort + ')" class="tts-divW"><div id="divTS' + sDir + '" class="tts-divT"></div><span id="spanTS' + sDir + '" class="tts-span">' + sSpanTitle + '</span></div>'

		var divTS = O('divTS' + sDir)
		var spanTS = O('spanTS' + sDir)
		var imgTS = O('imgTS' + sDir)

		divTtS.style.width = '40px'
		divTtS.style.height = '40px'

		//var nOffLR = 10
		//var nOffLR = ((sDir == 'a') ? nOffLR : -nOffLR)

		//divTtS.style.left = (divTt_rect.left + window.pageXOffset + (divTt_rect.width / 2) + nOffLR) + "px"
		//divTtS.style.top = (divTt_rect.top + window.pageYOffset + divTt_rect.height + 1) + "px"

		/*
		divTtS.style.left = (divTt_rect.left + window.pageXOffset) + "px"
		divTtS.style.top = (divTt_rect.top + window.pageYOffset + divTt_rect.height + 1) + "px"
		*/
		divTtS.style.left = (span_rect.left + window.pageXOffset) + "px"
		divTtS.style.top = (span_rect.top + window.pageYOffset + span_rect.height) + "px"

		divTtS.style.display = 'block'

		var spanTS_rect = spanTS.getBoundingClientRect()
		var imgTS_rect = imgTS.getBoundingClientRect()

		// race condition: image loading. so directly set here.
		imgTS_rect.width = 10
		imgTS_rect.width = 14

		var nPadTB = 8
		var nPadLR = 4
		var nPadTB2 = nPadTB * 2
		var nPadLR2 = nPadLR * 2

		//alert(imgTS_rect.width)
		//alert(imgTS_rect.height)

		//alert(spanTS_rect.width)
		//alert(spanTS_rect.height)

		//divTtS.style.width = (spanTS_rect.width + 6 + nPad2) + 'px'
		//divTtS.style.height = (spanTS_rect.height + 6 + nPad2) + 'px'
		//divTS.style.height = (nPad) + 'px'

		divTtS.style.width = (imgTS_rect.width + 6 + nPadLR2) + 'px'
		divTtS.style.height = (imgTS_rect.height + 6 + nPadTB2) + 'px'
		divTS.style.height = (nPadTB) + 'px'

		var divTtS_rect = divTtS.getBoundingClientRect()

		//if (sDir == 'd') {
		//    divTtS.style.left = (divTtS_rect.left - divTtS_rect.width) + 'px'
		//}

		//if (sDir == 'a') {
		//    divTtS.style.left = (divTtS_rect.left + divTtS_rect.width + 1) + 'px'
		//}

		//divSfDetails.style.left = (tdHeaderFld_rect.left + window.pageXOffset) + 'px'
		//divSfDetails.style.top = (tdHeaderFld_rect.top + window.pageYOffset) + 'px'

		if (sDir == 'a') {
			divTtS.style.top = (divTtS_rect.top + window.pageYOffset + divTtS_rect.height + 1) + 'px'
		}
		var nG = 20
		var nG2 = nG * 2
		divTtSG.style.left = (divTtS.style.left.replace('px', '') - nG) + 'px'
		divTtSG.style.top = (divTtS.style.top.replace('px', '') - nG) + 'px'
		divTtSG.style.width = (divTtS.style.width.replace('px', '') - -nG2) + 'px'
		divTtSG.style.height = (divTtS.style.height.replace('px', '') - -nG2) + 'px'
		divTtSG.style.display = 'block'
	} catch (ex) {SE(ex);throw ex}
}
function HideTt(ev) {
	try {
		$('#divTt').hide()
		$('#divTtSa').hide()
		$('#divTtSd').hide()
		$('#divTtSGa').hide()
		$('#divTtSGd').hide()
	} catch (ex) {SE(ex);throw ex}
}
function BoolChk(b) {
	try {
		var s = ''
		if (b) {
			s = '&#10003;'
		} else {
			s = ''
		}
		return s
	} catch (ex) {SE(ex);throw ex}
}
function BoolChkTd(b) {
	try {
		var s = ''
		if (b) {
			s = '&#10003;'
		} else {
			s = ' '
		}
		return s
	} catch (ex) {SE(ex);throw ex}
}
function IsBoolChk(s) {
	try {
		s = Trim (s)
		return s != ''
	} catch (ex) {SE(ex);throw ex}
}
function LockedHeader_Adjust(sSectName, sDivParentId) {
	try {
		//alert('OrderSel_AdjustHeader_()')

		var divHeadScroll = O(sSectName + '_divHeadScroll')
		var tblHead = O(sSectName + '_tblHead')

		var divDetScroll = O(sSectName + '_divDetScroll')
		var tblDet = O(sSectName + '_tblDet')

		var a_trDet = tblDet.getElementsByTagName('tr')
		var nTrDetCount = a_trDet.length
		if (nTrDetCount == 0) {
			return
		}

		var a_trHead = tblHead.getElementsByTagName('tr')
		var nTrHeadCount = a_trHead.length
		if (nTrHeadCount == 0) {
			return
		}

		var trDet = a_trDet[0]
		var trHead = a_trHead[0]

		var trDet_rect = trDet.getBoundingClientRect()
		var trHead_rect = trHead.getBoundingClientRect()

		var divDetScroll_rect = divDetScroll.getBoundingClientRect()
		var divHeadScroll_rect = divHeadScroll.getBoundingClientRect()

		var nOffX = 0
		var nOffY = 0

		var nPageOffX = 0
		var nPageOffY = 0

		if (sDivParentId != '') {
			var divParent = O(sDivParentId)
			var divParent_rect = divParent.getBoundingClientRect()
			nOffX = divParent_rect.left
			nOffY = divParent_rect.top
		} else {
			nPageOffX = window.pageXOffset
			nPageOffY = window.pageYOffset
		}

		/*
		if (bDb) {
			alert('divDetScroll_rect.top (' + divDetScroll_rect.top + ')')
			alert('nPageOffY (' + nPageOffY + ')')
			alert('nOffY (' + nOffY + ')')

			alert('divDetScroll_rect.left (' + divDetScroll_rect.left + ')')
			alert('nPageOffX (' + nPageOffX + ')')
			alert('nOffX (' + nOffX + ')')
		}
		*/

		divHeadScroll.style.top = (divDetScroll_rect.top + nPageOffY - nOffY) + 'px'
		divHeadScroll.style.left = (divDetScroll_rect.left + nPageOffX - nOffX) + 'px'
		divHeadScroll.style.height = (trDet_rect.height) + 'px'
		divHeadScroll.style.width = (divDetScroll_rect.width) + 'px'

		var a_tdDet = trDet.getElementsByTagName('td')
		var a_tdHead = trHead.getElementsByTagName('td')

		var nTdDetCount = a_tdDet.length
		var nTdHeadCount = a_tdHead.length

		for (var nTd = 0; nTd < nTdDetCount; nTd++) {
			var tdDet = a_tdDet[nTd]
			var tdHead = a_tdHead[nTd]

			var tdDet_rect = tdDet.getBoundingClientRect()

			tdHead.style.width = tdDet_rect.width + 'px'
			tdHead.style.minWidth = tdDet_rect.width + 'px'
		}
	} catch (ex) {SE(ex);throw ex}
}
function LockedHeader_OnHeadScroll(ev, sSectName) {
	try {
		var divHeadScroll = O(sSectName + '_divHeadScroll')
		var divDetScroll = O(sSectName + '_divDetScroll')
		divDetScroll.scrollLeft = divHeadScroll.scrollLeft
	} catch (ex) {SE(ex);throw ex}
}
function LockedHeader_OnDetScroll(ev, sSectName) {
	try {
		var divHeadScroll = O(sSectName + '_divHeadScroll')
		var divDetScroll = O(sSectName + '_divDetScroll')
		divHeadScroll.scrollLeft = divDetScroll.scrollLeft
	} catch (ex) {SE(ex);throw ex}
}
function Dlg_Open(sDivId, sCloseFunc) {
	try {
		var nImgW = 14
		var nImgH = 13
		var sDivCloseId = sDivId + 'Close'
		var div = O(sDivId)
		//div.style.display = 'block'
		$(div).append(
			'<div id="' + sDivCloseId + '" class="close-div z20" onclick="' + sCloseFunc + '()">' +
			'<img class="close-img" src="../img/close.png" alt="close" onmouseover="this.src=\'../img/close_s.png\'" onmouseout="this.src=\'../img/close.png\'" onmousedown="this.src=\'../img/close_d.png\'" onmouseup="this.src=\'../img/close.png\'" width="' + nImgW + '" height="' + nImgH + '" />' +
			'</div>'
		)
		divClose = O(sDivCloseId)
		var div_rect = div.getBoundingClientRect()
		divClose.style.width = (nImgW + 0) + 'px'
		divClose.style.height = (nImgH + 0) + 'px'
		divClose.style.left = (0 + 2) + 'px'
		divClose.style.top = (0 + 2) + 'px'
		$(divClose).show()
		var divClose_rect = divClose.getBoundingClientRect()
		divClose.style.left = (0 + div_rect.width - (divClose_rect.width + 4) - 2) + 'px'
	} catch (ex) {SE(ex);throw ex}
}
function Dlg_Close (sDivId) {
	try {
		var div = O(sDivId)
		$(div).hide()
	} catch (ex) {SE(ex);throw ex}
}
function Txt_Select (txt) {
	try {
		if (txt == null || txt.nodeType != Node.ELEMENT_NODE) {
			return false
		}
		if (txt.tagName != 'INPUT') {
			return false
		}
		txt.focus ()
		txt.select ()
		return true
	} catch (ex) {SE(ex);throw ex}
}
function Txt_UnSelect (txt)
{
	try {
		txt.selectionStart = -1
		txt.selectionEnd = -1
		txt.blur ()
	} catch (ex) {SE(ex);throw ex}
}
