// see www.whilewhat.com for info

// tr_<id>{_<id>}...
function Row_GetIdFromTr(tr) {
	try {
		//alert ('Row_GetIdFromTr(): tr (' + tr + ')')

		var a_sId = tr.id.split('_')
		var nCount = a_sId.length
		var a_nId = []
		for (var n = 1; n < nCount; n++) {
			var sId = a_sId[n]
			var nId = IntOr0 (sId)
			a_nId.push (nId)
		}
		//alert ('Row_GetIdFromTr() Ret: a_nId (' + a_nId + ')')
		return a_nId
	} catch (ex) {SE(ex);throw ex}
}
function Row_GetIdFromTd (td) {
	try {
		//alert ('Row_GetIdFromTd(): td (' + td + ')')

		if (td == null) {
			return null
		}
		var oElemCurr = td
		//alert ('tagName (' + oElemCurr.tagName + ')')
		while (oElemCurr != null) {
			if (oElemCurr.tagName == 'TR') {
				var a_nId = Row_GetIdFromTr(oElemCurr)
				return a_nId
			}
			oElemCurr = oElemCurr.parentElement
		}
		return null
	} catch (ex) {SE(ex);throw ex}
}
function Row_MakeTrName (a_nId)
{
	try {
		//alert ('Row_MakeTrName(): a_nId (' + a_nId + ')')

		var sTrName = 'tr'
		var nIdCount = a_nId.length
		for (var n = 0; n < nIdCount; n++) {
			var nId = a_nId[n]
			sTrName += '_' + nId
		}
		return sTrName
	} catch (ex) {SE(ex);throw ex}
}
function Row_MakeIdList (a_nId)
{
	try {
		//alert ('Row_MakeIdList(): a_nId (' + a_nId + ')')

		var sIdList = ''
		var nIdCount = a_nId.length
		for (var n = 0; n < nIdCount; n++) {
			var nId = a_nId[n]
			if (n > 0) {
				sIdList += '|'
			}
			sIdList += nId
		}
		return sIdList
	} catch (ex) {SE(ex);throw ex}
}
function Row_GetTrFromId(a_nId) {
	try {
		//alert ('Row_GetTrFromId(): a_nId (' + a_nId + ')')

		var sTrName = Row_MakeTrName (a_nId)
		var tr = O(sTrName)

		//alert ('Row_GetTrFromId() Ret: tr (' + tr + ')')
		return tr
	} catch (ex) {SE(ex);throw ex}
}
function Fld_GetTdFromElem (oElem) {
	try {
		//alert ('Fld_GetTdFromElem(): oElem (' + oElem + ')')

		if (oElem == null) {
			return null
		}
		var oElemCurr = oElem
		while (oElemCurr != null) {
			if (oElemCurr.tagName == 'TD') {
				return oElemCurr
			}
			oElemCurr = oElemCurr.parentElement
		}
		return null
	} catch (ex) {SE(ex);throw ex}
}
function Fld_GetFldIdFromTd (td) {
	try {
		//alert ('Fld_GetFldIdFromTd(): td (' + td + ')')

		var span = td.firstChild
		if (span == null) {
			return -1
		}
		var nFldId = GA(span, 'fs')
		if (nFldId == null) {
			return -1
		}
		return nFldId
	} catch (ex) {SE(ex);throw ex}
}
function Fld_GetFldIdFromHeadTd (td) {
	try {
		//alert ('Fld_GetFldIdFromHeadTd(): td (' + td + ')')

		var span = td.firstChild
		if (span == null || span.nodeType != Node.ELEMENT_NODE) {
			return -1
		}
		var sFldId = GA(span, 'fs')
		if (sFldId == null) {
			return -1
		}
		var nFldId = IntOr0(sFldId)
		return nFldId
	} catch (ex) {SE(ex);throw ex}
}
function Fld_GetFldIdFromEv (ev) {
	try {
		//alert ('Fld_GetFldIdFromEv(): ev (' + ev + ')')

		var oTarget = ev.target
		var nHeadY = 10
		var oElem = document.elementFromPoint (ev.pageX, nHeadY)
		if (oElem == null) {
			return false
		}
		var td = Fld_GetTdFromElem (oElem)
		if (td == null) {
			return false
		}
		var nFldId = Fld_GetFldIdFromHeadTd (td)

		return nFldId
	} catch (ex) {SE(ex);throw ex}
}

// globals: g_a_nIdSel, g_nFldIdSel
function Sel_SelRel (sSectName, nColRel, nRowRel)
{
	try {
		//alert ('Sel_SelRel(): sSectName (' + sSectName + ') nColRel (' + nColRel + ') nRowRel (' + nRowRel + ')')

		if (nColRel == 0 && nRowRel == 0) {
			return
		}
		var a_nId = g_a_nIdSel
		var nFldId = g_nFldIdSel
		if (nFldId < 0) {
			return
		}
		var tr = Row_GetTrFromId(a_nId)
		if (tr == null) {
			return
		}

		var a_FldParts = Fld_FindTdByFldId(sSectName, tr, nFldId)
		var spanFld = a_FldParts[0]
		var tdFldHead = a_FldParts[1]
		var tdFldBody = a_FldParts[2]

		var tdHead = tdFldHead

		if (nColRel != 0) {
			if (nColRel < 0) {
				tdHead = tdHead.previousElementSibling
			} else {
				tdHead = tdHead.nextElementSibling
			}
			if (tdHead != null) {
				nFldId = Fld_GetFldIdFromHeadTd (tdHead)
			}
		}
		if (nRowRel != 0) {
			if (nRowRel < 0) {
				tr = tr.previousElementSibling
				if (tr.className == 'trb SEP') {
					tr = tr.previousElementSibling
				}
				if (tr.className == 'trh') {
					tr = tr.previousElementSibling
				}
				while (tr != null) {
					if (tr.className.indexOf('dhid') == -1) {
						break
					}
					tr = tr.previousElementSibling
				}
			} else {
				tr = tr.nextElementSibling
				if (tr != null && tr.className == 'trb SEP') {
					tr = tr.nextElementSibling
				}
				while (tr != null) {
					if (tr.className.indexOf('dhid') == -1) {
						break
					}
					tr = tr.nextElementSibling
				}
			}
			if (tr != null) {
				a_nId = Row_GetIdFromTr(tr)
			}
		}
		Sel_Sel (sSectName, a_nId, nFldId, false)
	} catch (ex) {SE(ex);throw ex}
}

// OnKeyDown
// eg: <body onkeydown="Key_HandleArrowRet('EE', event)">
function Key_HandleArrowRet (sSectName, ev) {
	try {
		//alert ('Key_HandleArrowRet(): ev.keyCode (' + ev.keyCode + ')')

		var bHandle = false

		switch (event.keyCode) {
			case 38: // up
				Sel_SelRel (sSectName, 0, -1)
				bHandle = true
				break
			case 40: // down
			case 13: // enter
				Sel_SelRel (sSectName, 0, 1)
				bHandle = true
				break
			case 37: // left
				Sel_SelRel (sSectName, -1, 0)
				bHandle = true
				break
			case 39: // right
				Sel_SelRel (sSectName, 1, 0)
				bHandle = true
				break
		}

		if (bHandle) {
			ev.preventDefault()
		}
	} catch (ex) {SE(ex);throw ex}
}

// returns [span, tdHead, tdBody]
function Fld_FindTdByFldId(sSectName, trBody, nFldId)
{
	try {
		//alert ('Fld_FindTdByFldId(): sSectName (' + sSectName + ') trBody (' + trBody + ') nFldId (' + nFldId + ')')

		var sTblHeadName = sSectName + '_tblHead'

		var tblHead = O(sTblHeadName)

		var a_trHead = tblHead.getElementsByTagName('tr')
		var nTrHeadCount = a_trHead.length

		if (nTrHeadCount == 0) {
			return [null, null, null]
		}

		var trHead = a_trHead[0]

		var a_tdHead = trHead.getElementsByTagName('td')
		var nTdHeadCount = a_tdHead.length

		var nTdHead = -1
		var tdHead = null
		var span = null
		for (var nTd = 0; nTd < nTdHeadCount; nTd++) {
			tdHead = a_tdHead[nTd]

			var a_span = tdHead.getElementsByTagName('span')
			var nSpanCount = a_span.length

			if (nSpanCount != 1) {
				continue
			}
			span = a_span[0]
			var sFldId = GA(span, 'fs')
			if (sFldId == null) {
				continue
			}
			var nFldIdHead = IntOr0(sFldId)
			if (nFldIdHead == nFldId) {
				nTdHead = nTd
				break
			}
		}

		if (nTdHead == -1) {
			return [null, null, null]
		}

		var a_tdBody = trBody.getElementsByTagName('td')
		var nTdBodyCount = a_tdBody.length

		if (nTdBodyCount == 0) {
			return [span, tdHead, null]
		}

		var tdBody = a_tdBody[nTdHead]

		return [span, tdHead, tdBody]
	} catch (ex) {SE(ex);throw ex}
}

function Fld_ShowCurrCell(sSectName, tdFldHead, tdFldBody) {
	try {
		//alert ('Fld_ShowCurrCell(): sSectName (' + sSectName + ') tdFldHead (' + tdFldHead + ') tdFldBody (' + tdFldBody + ')')

		var tdFld = tdFldBody

		if (tdFld == null) {
			return
		}

		divCurrCell = O(sSectName + '_divCurrCell')

		var nOff = 1
		var nOff2 = nOff * 2

		var tblDet = O(sSectName + '_tblDet')
		var tblHead = O(sSectName + '_tblHead')

		var tblDet_rect = tblDet.getBoundingClientRect()
		var tblHead_rect = tblHead.getBoundingClientRect()
		var tdFld_rect = tdFld.getBoundingClientRect()

		var divDetScroll = O(sSectName + '_divDetScroll')

		var divDetScroll_rect = divDetScroll.getBoundingClientRect()

		var nL = tdFld_rect.left - tblDet_rect.left
		var nT = tdFld_rect.top - tblDet_rect.top
		var nR = tdFld_rect.right - tblDet_rect.left
		var nB = tdFld_rect.bottom - tblDet_rect.top

		var nW = nR - nL
		var nH = nB - nT

		divCurrCell.style.left = (nL + nOff) + "px"
		divCurrCell.style.top = (nT + nOff) + "px"

		divCurrCell.style.width = (nW - nOff2) + "px"
		divCurrCell.style.height = (nH - nOff2) + "px"

		divCurrCell.style.display = 'block'

		var nScrollY = 0
		var nScrollX = 0

		if (tdFld_rect.bottom > divDetScroll_rect.top + divDetScroll.clientHeight) {
			nScrollY = Math.ceil (
				(tdFld_rect.bottom - (divDetScroll_rect.top + divDetScroll.clientHeight)) /
				(divDetScroll_rect.height / 2))
		}
		if (tdFld_rect.right > divDetScroll_rect.left + divDetScroll.clientWidth) {
			nScrollX = Math.ceil (
				(tdFld_rect.right - (divDetScroll_rect.left + divDetScroll.clientWidth)) /
				(divDetScroll_rect.width / 2))
		}
		if (tdFld_rect.top < (divDetScroll_rect.top + tblHead_rect.height)) {
			nScrollY = -1
		}
		if (tdFld_rect.left < divDetScroll_rect.left) {
			nScrollX = -1
		}

		if (nScrollY != 0 || nScrollX != 0) {
			//EE_Adj ()
			//eval (sSectName + '_Adj ()')
			//window.EE_Adj ()
			//window['EE_Adj']()
			window[sSectName + '_Adj']()
		}

		if (nScrollY != 0) {
			divDetScroll.scrollTop += (divDetScroll_rect.height / 2) * nScrollY
		}
		if (nScrollX != 0) {
			divDetScroll.scrollLeft += (divDetScroll_rect.width / 2) * nScrollX
		}

		Fld_FocusIfEdit(tdFldHead, tdFldBody)
	} catch (ex) {SE(ex);throw ex}
}

function Fld_HideCurrCell(tdFld) {
	try {
		//alert ('Fld_HideCurrCell(): tdFld (' + tdFld + ')')

		divCurrCell = O('divCurrCell')
		digvCurrCell.style.display = 'none'
	} catch (ex) {SE(ex);throw ex}
}

function Fld_HasTdFldOption (tdFldHead, sOpt) {
	try {
		//alert ('Fld_HasTdFldOption(): tdFldHead (' + tdFldHead + ') sOpt (' + sOpt + ')')

		if (tdFldHead == null) {
			return false
		}
		var sOptList = GA (tdFldHead, "o")
		if (sOptList == null) {
			return false
		}
		if (sOptList.indexOf (sOpt) != -1) {
			return true
		}
		return false
	} catch (ex) {SE(ex);throw ex}
}
function Fld_FocusIfEdit(tdFldHead, tdFldBody) {
	try {
		//alert ('Fld_FocusIfEdit(): tdFldHead (' + tdFldHead + ') tdFldBody (' + tdFldBody + ')')

		var txt = tdFldBody.firstChild
		if (txt == null || txt.nodeType != Node.ELEMENT_NODE) {
			return false
		}
		if (txt.tagName != 'INPUT') {
			return false
		}
		txt.focus ()
		if (Fld_HasTdFldOption(tdFldHead, "S")) {
			txt.select ()
		}
		return true
	} catch (ex) {SE(ex);throw ex}
}

function Sel_S (sSectName, ev, bDblClick)
{
	try {
		//alert ('Sel_S(): sSectName (' + sSectName + ') ev (' + ev + ') bDblClick (' + bDblClick + ')')

		var tr = ev.currentTarget

		var a_nId = Row_GetIdFromTr(tr)
		var nFldId = Fld_GetFldIdFromEv (ev)

		Sel_Sel (sSectName, a_nId, nFldId, bDblClick)
	} catch (ex) {SE(ex);throw ex}
}
function Sel_TrSel (a_nId, nFldId) {
	try {
		//alert ('Sel_TrSel(): a_nId (' + a_nId + ') nFldId (' + nFldId + ')')

		var sTrNameSel = Row_MakeTrName (g_a_nIdSel)
		//alert ('sTrNameSel (' + sTrNameSel + ')')
		SetTrSel(sTrNameSel, false)
		g_a_nIdSel = a_nId
		var sTrNameSel = Row_MakeTrName (g_a_nIdSel)
		//alert ('sTrNameSel (' + sTrNameSel + ')')
		var bRes = SetTrSel(sTrNameSel, true)
		g_nFldIdSel = nFldId
		return bRes
	} catch (ex) {SE(ex);throw ex}
}
function Sel_Sel (sSectName, a_nId, nFldId, bDblClick) {
	try {
		//alert (typeof (a_nId))
		if (typeof (a_nId) == "unknown") {
			a_nId = a_nId.toArray()
		}
		//alert ('Sel_Sel(): sSectName (' + sSectName + ') a_nId (' + a_nId + ') nFldId (' + nFldId + ') bDblClick (' + bDblClick + ')')

		if (nFldId < 10) {
			nFldId = 10
		}

		Sel_TrSel (a_nId, nFldId)

		var tr = Row_GetTrFromId (a_nId)

		//alert ('tr (' + tr + ')')
		//alert ('tr.outerHTML (' + tr.outerHTML + ')')

		var sDataList = ''

		if (tr == null) {
			return
		}

		var a_FldParts = Fld_FindTdByFldId(sSectName, tr, nFldId)
		var spanFld = a_FldParts[0]
		var tdFldHead = a_FldParts[1]
		var tdFldBody = a_FldParts[2]

		var td = tdFldBody

		//alert ('td (' + td + ')')
		//alert ('td.outerHTML (' + td.outerHTML + ')')

		Fld_ShowCurrCell(sSectName, tdFldHead, tdFldBody)

		if (tr != null) {
			sDataList = GA(tr, 'd')
		}
		Sel_DoEvSel (bDblClick, sDataList, nFldId)

		//alert ('Sel_Sel() Ret: X')
	} catch (ex) {SE(ex);throw ex}
}
function Sel_SelFld(sSectName, a_nRowId, nFldId) {
	try {
		//alert (typeof (a_nRowId))
		if (typeof (a_nRowId) == "unknown") {
			a_nRowId = a_nRowId.toArray()
		}
		//alert ('Sel_SelFld(): sSectName (' + sSectName + ') a_nRowId (' + a_nRowId + ') nFldId (' + nFldId + ')')

		var tr = Row_GetTrFromId (a_nRowId)

		if (tr == null) {
			return
		}

		var a_FldParts = Fld_FindTdByFldId(sSectName, tr, nFldId)
		var spanFld = a_FldParts[0]
		var tdFldHead = a_FldParts[1]
		var tdFldBody = a_FldParts[2]

		//alert ('Fld_FindTdByFldId() Ret: spanFld (' + spanFld + ') tdFldHead (' + tdFldHead + ') tdFldBody (' + tdFldBody + ')')

		if (tdFldBody == null) {
			return
		}

		var txt = tdFldBody.firstChild
		Txt_Select (txt)
	} catch (ex) {SE(ex);throw ex}
}
function Sel_DoEvSel (bDblClick, sDataList, nFldId)
{
	try {
		//alert ('Sel_DoEvSel(): bDblClick (' + bDblClick + ') sDataList (' + sDataList + ') nFldId (' + nFldId + ')')

		var sEv = 'S'

		hid_sEv = O('ev_sEv')
		hid_bDblClick = O('ev_bDblClick')
		hid_sDataList = O('ev_sDataList')
		hid_nFldId = O('ev_nFldId')

		hid_sEv.value = sEv
		hid_bDblClick.value = Int01(bDblClick)
		hid_sDataList.value = sDataList
		hid_nFldId.value = nFldId

		ev_butEv.click()
	} catch (ex) {SE(ex);throw ex}
}

function Chg_C (ev) {
	try {
		//alert ('Chg_C(): ev (' + ev + ')')

		var txt = ev.target
		var td = txt.parentElement
		var tr = td.parentElement
		//Txt_UnSelect (txt)
		//K (ev)
		Chg_MarkTxtFld (td, txt)
		Chg_AddRowChange (tr)
	} catch (ex) {SE(ex);throw ex}
}
function Chg_K (ev) {
	try {
		//alert ('Chg_K(): ev (' + ev + ')')

		var txt = ev.target

		switch (event.keyCode) {
			case 13: // enter
			case 38: // up
			case 40: // down
			case 37: // left
			case 39: // right
				//alert ('K(enter)')
				ev.preventDefault()
				//txt.blur ()
				Txt_UnSelect (txt)
				break
		}
	} catch (ex) {SE(ex);throw ex}
}
function Chg_RowHasChanges (tr) {
	try {
		//alert ('Chg_RowHasChanges(): tr (' + tr + ')')

		var a_td = tr.getElementsByTagName('td')
		var nTdCount = a_td.length
		for (var nTd = 0; nTd < nTdCount; nTd++) {
			var sClass = a_td[nTd].className
			//alert ('sClass (' + sClass + ')')
			if (sClass.indexOf('diff-k') > -1 ||
				sClass.indexOf('diff-e') > -1) {
				return true
			}
		}
		return false
	} catch (ex) {SE(ex);throw ex}
}
function Chg_AddRowChange (tr) {
	try {
		//alert ('Chg_AddRowChange(): tr (' + tr + ')')

		var bRowHasChanges = Chg_RowHasChanges (tr)
		tr.setAttribute('c', bRowHasChanges ? '1' : '0')
		var sIdList = tr.getAttribute('i')
		if (bRowHasChanges) {
			g_a_sIdListChange[sIdList] = 1
		} else {
			delete g_a_sIdListChange[sIdList]
		}
		Chg_UpdateSaveCancel (sIdList)
	} catch (ex) {SE(ex);throw ex}
}
function Chg_AddAllRowChanges (sSectName, a_nRowId) {
	try {
		//alert (typeof (a_nRowId))
		if (typeof (a_nRowId) == "unknown") {
			a_nRowId = a_nRowId.toArray()
		}
		//alert ('Chg_AddAllRowChanges(): sSectName (' + sSectName + ') a_nRowId (' + a_nRowId + ')')

		var tbl = O(sSectName + '_tblDet')
		var a_tr = tbl.getElementsByTagName('tr')
		var nTrCount = a_tr.length
		for (var nTr = 0; nTr < nTrCount; nTr++) {
			var tr = a_tr[nTr]

			var bRowHasChanges = Chg_RowHasChanges (tr)
			tr.setAttribute('c', bRowHasChanges ? '1' : '0')
			var sIdList = tr.getAttribute('i')
			if (bRowHasChanges) {
				g_a_sIdListChange[sIdList] = 1
			} else {
				delete g_a_sIdListChange[sIdList]
			}
		}
		//Chg_UpdateSaveCancel (sIdList)
		Chg_UpdateSaveCancel ('')
	} catch (ex) {SE(ex);throw ex}
}
function Chg_HaveChanges() {
	try {
		//alert ('Chg_HaveChanges()')

		var bChanges = !AssocArrEmpty (g_a_sIdListChange)
		return bChanges
	} catch (ex) {SE(ex);throw ex}
}
function Chg_UpdateSaveCancel (sIdList) {
	try {
		//alert ('Chg_UpdateSaveCancel(): sIdList (' + sIdList + ')')

		var bChanges = Chg_HaveChanges()
		Chg_DoEvUsc (sIdList, bChanges)
	} catch (ex) {SE(ex);throw ex}
}
function Chg_DoEvUsc (sIdList, bChanges) {
	try {
		//alert ('Chg_DoEvUsc(): sIdList (' + sIdList + ') bChanges (' + bChanges + ')')

		var sEv = 'USC'

		butEv = O('ev_butEv')

		hid_sEv = O('ev_sEv')
		hid_sIdList = O('ev_sIdList')
		hid_bChanges = O('ev_bChanges')

		hid_sEv.value = sEv
		hid_sIdList.value = sIdList
		hid_bChanges.value = Int01(bChanges)

		butEv.click()
	} catch (ex) {SE(ex);throw ex}
}

function Menu_OnTblCtxMenu(sSectName, ev) {
	try {
		//alert ('Menu_OnTblCtxMenu(): sSectName (' + sSectName + ') ev (' + ev + ')')

		if (ev.ctrlKey) {
			return true
		}
		var oElem = document.elementFromPoint (ev.pageX, ev.pageY)
		if (oElem == null) {
			return false
		}
		var td = Fld_GetTdFromElem (oElem)
		if (td == null) {
			return false
		}

		var divHeadScroll = O(sSectName + '_divHeadScroll')
		var divDetScroll = O(sSectName + '_divDetScroll')

		var a_nId = Row_GetIdFromTd (td)
		var sIdList = Row_MakeIdList (a_nId)

		//alert ('sIdList (' + sIdList + ')')

		if (sIdList == '') {
			var nCellX = td.offsetLeft
			var nCellY = td.offsetTop
		} else {
			var nCellX = td.offsetLeft - divHeadScroll.scrollLeft
			var nCellY = td.offsetTop - divDetScroll.scrollTop
		}
		var nCellW = td.offsetWidth
		var nCellH = td.offsetHeight

		var oTarget = ev.target
		var nHeadY = 10
		var oElem = document.elementFromPoint (ev.pageX, nHeadY)
		if (oElem == null) {
			return false
		}
		var td = Fld_GetTdFromElem (oElem)
		if (td == null) {
			return false
		}
		var nFldId = Fld_GetFldIdFromTd (td)

		Sel_Sel (sSectName, a_nId, nFldId, false)
		Menu_OnTblCtxMenuData(sIdList, nFldId, nCellX, nCellY, nCellW, nCellH)

		return false
	} catch (ex) {SE(ex);throw ex}
}
function Menu_OnTblCtxMenuData(sIdList, nFldId, nCellX, nCellY, nCellW, nCellH) {
	try {
		//alert ('Menu_OnTblCtxMenuData(): sIdList (' + sIdList + ') nFldId (' + nFldId + ') nCellX (' + nCellX + ') nCellY (' + nCellY + ') nCellW (' + nCellW + ') nCellH (' + nCellH + ')')

		butEv = O('ev_butEv')

		hid_sEv = O('ev_sEv')

		hid_bDblClick = O('ev_bDblClick')
		hid_sIdList = O('ev_sIdList')
		hid_nFldId = O('ev_nFldId')
		hid_nCellX = O('ev_nCellX')
		hid_nCellY = O('ev_nCellY')
		hid_nCellW = O('ev_nCellW')
		hid_nCellH = O('ev_nCellH')

		hid_sEv.value = 'CM'

		hid_bDblClick.value = 0
		hid_sIdList.value = sIdList
		hid_nFldId.value = nFldId
		hid_nCellX.value = nCellX
		hid_nCellY.value = nCellY
		hid_nCellW.value = nCellW
		hid_nCellH.value = nCellH

		butEv.click()
	} catch (ex) {SE(ex);throw ex}
}
